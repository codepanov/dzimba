import axios from 'axios'

let baseURL = process.env.VUE_APP_APIBASEURL
let instance = axios.create({
  baseURL: baseURL
})

export default instance;