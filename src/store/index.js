import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)



export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    user_loged: false,
    user_obj: {
      user_id: null,
      username: null,
      gender: null,
      level: null,
      age: null,
      bmi: null,
      height: null,
      weight: null,
      body_fat: null
    },
    selected_exercises: [],
    affected_muscle_group: null,
    avatar: null
  },
  mutations: {
    CHECK_USER_STATUS(state) {
      return state.user_loged // not finished!
    },
    ADD_SELECTED_EXERCISES(state, payload) {
      state.selected_exercises = payload.vezbe
    },
    SET_AVATAR(state, payload) {
      state.avatar = payload.picture
    },
    REMOVE_AVATAR(state) {
      state.avatar = null
    },
    CLEAR_SELECTED_EXERCISES(state) {
      state.selected_exercises = []
    },
    ADD_AFFECTED_MUSCLE_GROUP(state, payload) {
      state.affected_muscle_group = payload.muscle
    },
    CLEAR_AFFECTED_MUSCLE_GROUP(state) {
      state.affected_muscle_group = []
    },
    UPDATE_USERNAME(state, payload) {
      state.user_obj.username = payload.username
    },
    UPDATE_BODY_STATS(state, payload) {
      state.user_obj.height = payload.height
      state.user_obj.weight = payload.weight
    },
    SET_USER_LOGED(state, payload) {
      state.user_loged = true
      state.user_obj.user_id = payload.user_id
      state.user_obj.username = payload.username
      state.user_obj.gender = payload.gender
      state.user_obj.level = payload.level
      state.user_obj.age = payload.age
      state.user_obj.bmi = payload.bmi
      state.user_obj.height = payload.height
      state.user_obj.weight = payload.weight
      state.user_obj.body_fat = payload.body_fat
      state.selected_exercises = []
      // state.affected_muscle_group = null
    },
    SET_USER_LOGEDOUT(state) {
      state.user_loged = false,
      state.user_obj.user_id = null
      state.user_obj.username = null
      state.user_obj.gender = null
      state.user_obj.level = null
      state.user_obj.age = null
      state.user_obj.bmi = null
      state.user_obj.height = null
      state.user_obj.weight = null
      state.user_obj.body_fat = null
      state.selected_exercises = []
      state.affected_muscle_group = null
    }
  },
  actions: {
    // set_user_loged(context, payload) {
    //   context.commit('SET_USER_LOGED', payload)
    // }
  },
  modules: {
    
  }
})
