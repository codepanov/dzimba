import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    beforeEnter: (to, from, next) => {
      if(store.state.user_loged === true) {
        next('/profile')
      }
      else next()
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/user_auth/Login.vue'),
    beforeEnter: (to, from, next) => {
      if (store.state.user_loged === true) {
        next('/profile')
      }
      else next()
    }
  },
  {
    path: '/get_started',
    name: 'get_started',
    component: () => import('../views/user_auth/Get_started.vue')
  },
  {
    path: '/exercises',
    name: 'exercises',
    component: () => import('../views/exercises/Exercises.vue')
  },
  {
    path: '/start-workout',
    name: 'start-workout',
    component: () => import('../views/exercises/Start_workout.vue')
  },
  {
    path: '/workout',
    name: 'workout',
    component: () => import('../views/user_pages/Workout.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('../views/user_pages/Profile.vue')
  },
  {
    path: '/user-stats',
    name: 'user-stats',
    component: () => import('../views/user_pages/User_stats.vue')
  },
  {
    path: '/choose_groups',
    name: '/choose_groups',
    component: () => import('../views/user_pages/Choose_groups.vue')
  },
  {
    path: '/nutrition',
    name: 'nutrition',
    component: () => import('../views/user_pages/Nutrition.vue')
  },
  {
    path: '/meals/:category_id',
    name: 'meals',
    component: () => import('../views/user_pages/Meals.vue'),
    props: route => Object.assign({}, route.query, route.params) // dodao Danilo za id category prop
  },
  {
    path: '/recepie/:meal_id',
    name: 'recepie',
    component: () => import('../views/user_pages/Recepie.vue'),
    props: route => Object.assign({}, route.query, route.params) // dodao Danilo za id meal prop
  },
  // {
  //   path: '*',
  //   name: '404',
  //   component: () => import('../views/not_found/Not_found.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// router.beforeEach((to, from, next) => {
//   if(isAuthenticated) {
//     next()
//   } else 
//     next('/')
// })

export default router
