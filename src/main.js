import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

// Filter mora iznad instance
Vue.filter('two_digits', function (value) {
  if(value.toString().length <= 1) {
    return "0" + value.toString();
  }
  return value.toString();
});


export const bus = new Vue()

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
